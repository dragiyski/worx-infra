(function () {
    'use strict';

    const List = require('./List');

    const symbols = Object.freeze({
        set: Symbol('set')
    });

    function OrderedSet() {
        List.call(this);
        this[symbols.set] = new Set();
    }

    OrderedSet.prototype = Object.create(List.prototype, {
        constructor: {
            value: OrderedSet
        },
        append: {
            value: function (item) {
                if (!this[symbols.set].has(item)) {
                    this[symbols.set].add(item);
                    List.prototype.append.call(this, item);
                }
            }
        },
        prepend: {
            value: function (item) {
                if (!this[symbols.set].has(item)) {
                    this[symbols.set].add(item);
                    List.prototype.prepend.call(this, item);
                }
            }
        },
        replace: {
            value: function (item, replacement) {
                if (this[symbols.set].has(item) || this[symbols.set].has(replacement)) {
                    const doReplace = (array, index) => {
                        array[index] = replacement;
                        action = doRemove;
                    };
                    const doRemove = (array, index) => {
                        array.splice(index, 1);
                    };
                    let action = doReplace;
                    for (let index = 0; index < this[List.symbols.list].length; ++index) {
                        const listItem = this[List.symbols.list][index];
                        if (listItem === item || listItem === replacement) {
                            action(this[List.symbols.list], index);
                        }
                    }
                    this[symbols.set].delete(item);
                    this[symbols.set].add(replacement);
                }
            }
        },
        contains: {
            value: function (item) {
                return this[symbols.set].has(item);
            }
        },
        isSubsetOf: {
            value: function (orderedSet) {
                if (!(orderedSet instanceof OrderedSet)) {
                    return false;
                }
                for (const item of this) {
                    if (!orderedSet.contains(item)) {
                        return false;
                    }
                }
                return true;
            }
        },
        isSupersetOf: {
            value: function (orderedSet) {
                if (!(orderedSet instanceof OrderedSet)) {
                    return false;
                }
                return orderedSet.isSubsetOf(this);
            }
        },
        clone: {
            value: function (target = null) {
                if (target == null) {
                    target = Object.create(OrderedSet.prototype);
                }
                List.prototype.clone.call(this, target);
                target[symbols.set] = new Set(this[symbols.set]);
                return target;
            }
        },
        intersection: {
            value: function (orderedSet) {
                if (!(orderedSet instanceof OrderedSet)) {
                    return this.clone();
                }
                const result = new OrderedSet();
                for (const item of this) {
                    if (orderedSet.contains(item)) {
                        result.append(item);
                    }
                }
                return result;
            }
        },
        union: {
            value: function (orderedSet) {
                if (!(orderedSet instanceof OrderedSet)) {
                    return this.clone();
                }
                const result = new OrderedSet();
                for (const item of this[symbols.list]) {
                    result.append(item);
                }
                for (const item of orderedSet) {
                    result.append(item);
                }
                return result;
            }
        }
    });
})();
