(function () {
    'use strict';

    const symbols = Object.freeze({
        list: Symbol('list')
    });

    function List() {
        this[symbols.list] = [];
    }

    List.prototype = Object.create(Object.prototype, {
        constructor: {
            value: List
        },
        item: {
            value: function (index) {
                if (typeof index !== 'number' || index !== parseInt(index) || index < 0 || index >= this[symbols.list].length) {
                    return;
                }
                return this[symbols.list][index];
            }
        },
        append: {
            value: function (item) {
                this[symbols.list].push(item);
            }
        },
        prepend: {
            value: function (item) {
                this[symbols.list].unshift(item);
            }
        },
        extend: {
            value: function (iterable) {
                for (const item of iterable) {
                    this.append(item);
                }
            }
        },
        replace: {
            value: function (condition, item) {
                for (let i = 0; i < this[symbols.item].length; ++i) {
                    if (condition(this[symbols.item])) {
                        this[symbols.item][i] = item;
                    }
                }
            }
        },
        contains: {
            value: function (item) {
                return this[symbols.list].indexOf(item) >= 0;
            }
        },
        insert: {
            value: function (index, item) {
                this[symbols.list].splice(index, 0, item);
            }
        },
        delete: {
            value: function (condition) {
                this[symbols.list] = this[symbols.list].filter((item) => {
                    return !condition(item);
                });
            }
        },
        empty: {
            value: function () {
                this[symbols.list].length = 0;
            }
        },
        size: {
            get: function () {
                return this[symbols.list].length;
            }
        },
        isEmpty: {
            value: function () {
                return this[symbols.list].length === 0;
            }
        },
        clone: {
            value: function (target = null) {
                if (target == null) {
                    target = Object.create(List.prototype);
                }
                target[symbols.list] = this[symbols.list].slice(0);
                return target;
            }
        },
        [Symbol.iterator]: {
            value: function * () {
                for (const item of this[symbols.list]) {
                    yield item;
                }
            }
        },
        [Symbol.toStringTag]: {
            value: 'List'
        }
    });

    Object.defineProperties(List, {
        symbols: {
            value: symbols
        }
    });

    module.exports = List;
})();
